# Editing
export EDITOR='vim'
export VISUAL="$EDITOR"
export LESS="-RM~gIsw"
alias vi="vim"
